import * as Koa from 'koa';
import * as serveStatic from 'koa-static';
import * as session from 'koa-session';
import *  as path from 'path';
import router from './router';
import global from './globals';

const app = new Koa();
const PORT = global.port;

const CONFIG = {
  maxAge: 86400000,
};

app.use(session(CONFIG, app));

if (process.env.NODE_ENV === 'production') {
  const appPath = path.resolve('..', 'app', 'build');
  app.use(serveStatic(appPath));
}

app.use(router.routes());

app.use((ctx) => {
  let n = ctx.session.view || 0;
  ctx.session.views = ++n;
  console.log(`${n} views`);
});

app.listen(PORT);

console.log(`server running: http://localhost:${PORT}`);
