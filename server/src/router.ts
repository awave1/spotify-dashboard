import * as Router from 'koa-router';
import * as querystring from 'querystring';
import * as request from 'request-promise-native';
import axios from 'axios';
import global from './globals';
import { accounts, api } from './spotifyApi';
import { getRandomString } from './utils';

const router = new Router();

// @TODO: ideally this shouldn't be here?
router.get('/', (ctx) => {
  // redirect to react app server
  if (process.env.NODE_ENV !== 'production') {
    ctx.redirect(`http://localhost:3000/?${querystring.stringify(ctx.query)}`);
  }
});

router.get('/api/login', (ctx) => {
  const state = getRandomString();
  ctx.cookies.set(global.appAuthStateKey, state);

  // @TODO: refactor this into a function or something more generic
  const scope = 'user-read-private user-library-read user-library-modify playlist-read-private';

  const queryParams = querystring.stringify({
    response_type: 'code',
    client_id: global.clientId,
    scope,
    redirect_uri: global.redirectUrl,
    state,
    show_dialog: true,
  });

  ctx.redirect(accounts(`/authorize?${queryParams}`));
});

router.get('/api/callback', async (ctx) => {
  const { code, state } = ctx.query;
  const storedState = ctx.cookies.get(global.appAuthStateKey);

  if (code && state && state === storedState) {
    // clear cookie
    ctx.cookies.set(global.appAuthStateKey, null);

    const resp = await requestAuth(code);
    if (resp) {
      ctx.body = 'success';
      ctx.redirect(`/?${querystring.stringify(resp)}`);
    }
  }
});

// @TODO clean up this shit code
const requestAuth = async (code) => {
  const form = {
    code,
    redirect_uri: global.redirectUrl,
    grant_type: 'authorization_code',
  };

  const authStr = Buffer.from(global.clientId + ':' + global.clientSecret).toString('base64');

  const authOptions = {
    method: 'post',
    uri: accounts('/api/token'),
    form,
    headers: {
      Authorization: `Basic ${authStr}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    json: true,
  };

  // @TODO: error handling
  try {
    const {
      access_token: accessToken,
      refresh_token: refreshToken,
    } = await request.post(authOptions);

    return {
      accessToken,
      refreshToken,
    };
  } catch (err) {
    console.log(err);
  };

  return undefined;
};

router.get('/api/refreshToken', (ctx) => {
  const { refreshToken } = ctx.query;
  const authOptions = {
    headers: {
      Authorization: `Basic ${Buffer.from(global.clientId + ':' + global.clientSecret).toString('base64')}`,
    },
  };

  const data = {
    grant_type: 'authorization_code',
      refresh_token: refreshToken,
  };

  axios.post(accounts('/api/token'), data, authOptions).then(({data}) => {
    const { access_token: accessToken } = data;
    //ctx.body = { access_token: accessToken };
    ctx.redirect(`/?access_token=${accessToken}`)
  });
});

export default router;
