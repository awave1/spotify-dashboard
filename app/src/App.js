import React, { Component } from 'react';
import axios from 'axios';
import queryString from 'query-string';
import Routes from './Routes';
import Toolbar from './components/Toolbar';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    const {
      query: { accessToken, refreshToken }
    } = queryString.parseUrl(window.location.href);

    this.state = {
      session: 'EMPTY',
      accessToken,
      refreshToken,
      loggedIn: accessToken || false,
    };
  }

  render() {
    const { loggedIn } = this.state;
    return (
      <div className="App">
        <Toolbar/>
        <Routes/>
      </div>
    );
  }
}

export default App;
