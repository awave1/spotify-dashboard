import MainPage from './MainPage';
import DashboardPage from './DashboardPage';

export {
  MainPage,
  DashboardPage,
};
